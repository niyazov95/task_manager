export interface IPage {
  _id?: string;
  title: string;
  route: string;
}
