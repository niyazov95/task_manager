import { ILanguage } from './language.interface';

export interface IValuesChanges {
  from: string;
  to: string;
  languageId: string | ILanguage;
}
