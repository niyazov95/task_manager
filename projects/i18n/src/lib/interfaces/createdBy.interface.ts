import { IUser } from './user.interface';

export interface ICreatedBy {
  time: number;
  user: UserT;
}

export type UserT = IUser | string;
