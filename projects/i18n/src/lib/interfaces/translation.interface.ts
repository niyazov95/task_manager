import {ITranslationPage} from './translation-page.interface';
import { ILanguage } from './language.interface';

export interface ITranslation {
  languageId: string | ILanguage;
  pages: ITranslationPage[];
  visible?: boolean;
}
