export enum UpdatedByActionE {
  AddedTranslation = 0,
  UpdatedTranslation = 1,
  AddedPage = 2,
  UpdatedPageTranslation = 3,
}
