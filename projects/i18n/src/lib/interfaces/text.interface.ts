import {ITranslation} from './translation.interface';
import { ICreatedBy } from './createdBy.interface';
import { IUpdatedBy } from './updatedBy.interface';

export interface IText {
  _id: string;
  title: string;
  translations: ITranslation[];
  createdBy: ICreatedBy;
  updatedBy: IUpdatedBy[];
}
