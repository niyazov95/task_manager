export interface IGetTextResponse {
  _id: string;
  value: string;
}
