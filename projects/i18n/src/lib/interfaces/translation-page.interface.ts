import {IPage} from './page.interface';

export interface ITranslationPage {
  page: string | IPage;
  value: string;
}
