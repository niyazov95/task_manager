export interface ILanguage {
  readonly _id: string;
  readonly title: string;
  readonly description: string;
  readonly url: string;
  readonly active: boolean;
  readonly default: boolean;
}
