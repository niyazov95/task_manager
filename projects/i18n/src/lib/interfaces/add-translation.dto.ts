import { IPage } from './page.interface';

export class AddTranslationDto {
  page: IPage;
  value: string;
}
