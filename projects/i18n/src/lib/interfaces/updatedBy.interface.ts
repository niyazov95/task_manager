import { IUser } from './user.interface';
import { IValuesChanges } from './values.changes.interface';
import { UpdatedByActionE } from './updatedBy.action.enum';

export interface IUpdatedBy {
  action: UpdatedByActionE;
  user: string | IUser;
  time: number;
  values: IValuesChanges;
}
