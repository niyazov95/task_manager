import { Component } from '@angular/core';
import { DevelopmentService } from '../../../../../src/modules/development/development.service';
import { I18nService } from '../i18n.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent {
  hide = true;
  userName: string;
  password: string;

  constructor(private developmentService: DevelopmentService, private i18nService: I18nService) {}

  login() {
    this.developmentService.loginAsEditor(this.userName, this.password).subscribe((r: any) => {
      console.log(r.data);
      localStorage.setItem('token', r.data.token);
      localStorage.setItem('loggedAsEditor', 'true');
      this.developmentService.loggedAsEditor = true;
      this.developmentService.loginState.next(true);
    });
    // this.developmentService.getLoginState();
    // .subscribe(
    //   (response: any) => console.log(response),
    //   (err: any) => console.log(err)
    // );
  }
}
