import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';
import { I18nService } from './i18n.service';
import { ActivatedRoute } from '@angular/router';
import { IText } from './interfaces/text.interface';
import { UIService } from './services/ui.service';
import { TooltipComponent } from './tooltip/tooltip.component';
import { DevelopmentService } from '../../../../src/modules/development/development.service';

@Directive({
  selector: '[localization]'
})
export class I18nDirective implements OnInit {
  id: string;
  initialText: string;
  tooltip = document.getElementById('tooltip') as HTMLElement;
  loggedAsDev: boolean;
  constructor(
    private i18nService: I18nService,
    private uiService: UIService,
    private element: ElementRef<HTMLElement>,
    private route: ActivatedRoute,
    private developmentService: DevelopmentService
  ) {}
  ngOnInit(): void {
    this.initDirective();
    this.i18nService.onLanguageChange.subscribe(sub => this.initDirective());
    this.i18nService.onDataUpdate.subscribe(sub => this.initDirective());
  }
  initDirective() {
    setTimeout(() => {
      const { path } = this.route.routeConfig;
      const { name } = this.route.routeConfig.component;
      if (!this.initialText) {
        this.initialText = this.element.nativeElement.innerText;
      }
      const { _id, value } = this.i18nService.getText(this.initialText, {
        title: name,
        route: path
      });
      if (!_id) {
        this.i18nService
          .createText({ title: this.initialText }, { title: name, route: path })
          .subscribe((text: IText) => {
            this.i18nService.addText__sub();
          });
      } else {
        this.id = _id;
        this.setElementText(value);
      }
      if (!this.tooltip) {
        this.uiService.addTooltip<TooltipComponent>(TooltipComponent);
        this.tooltip = document.getElementById('tooltip') as HTMLElement;
      }
    }, 1);
  }
  @HostListener('mouseenter') onMouseEnter() {
    const rect = this.element.nativeElement.getBoundingClientRect();
    // console.log(rect);

    this.tooltip.getElementsByTagName('p')[0].innerText = this.initialText;

    if (this.developmentService.getLoginState()) {
      this.tooltip.style.visibility = 'visible';
      this.tooltip.style.opacity = '1';
      if (rect.top < 50) {
        this.tooltip.style.top = `${rect.top + 30}px`;
      } else {
        this.tooltip.style.top = `${rect.top - 30}px`;
      }

      this.tooltip.style.left = `${rect.left}px`;
      this.tooltip.setAttribute('textId', this.id);
      setTimeout(() => {
        this.tooltip.style.visibility = 'hidden';
        this.tooltip.style.opacity = '0';
      }, 5000);
    } else if (this.tooltip.style.visibility === 'visible') {
      this.tooltip.style.visibility = 'hidden';
      this.tooltip.style.opacity = '0';
    }
  }
  setElementText(value: string) {
    setTimeout(() => (this.element.nativeElement.innerText = value), 1);
  }
}
