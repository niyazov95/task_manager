import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UIService } from '../services/ui.service';
import { TranslateModalComponent } from '../translate-modal/translate-modal.component';

@Component({
  selector: 'lib-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']

})
export class TooltipComponent implements OnInit {
  @ViewChild('translateText', { static: false }) translateText: ElementRef<HTMLElement>;
  constructor(
    private uiService: UIService
 ) { }

  ngOnInit() {}

  openModal() {
    const id = document.getElementById('tooltip').getAttribute('textId');
    this.uiService.openModal<TranslateModalComponent>(TranslateModalComponent, id);
  }

}
