import { ModuleWithProviders, NgModule } from '@angular/core';
import { I18nService } from './i18n.service';
import { I18nDirective } from './i18n.directive';
import { LocalizationConfig } from './classes/localization.config';
import { TooltipComponent } from './tooltip/tooltip.component';
import { TranslateModalComponent } from './translate-modal/translate-modal.component';
import { UIService } from './services/ui.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../../src/modules/material';
import { DevelopmentModule } from '../../../../src/modules/development/development.module';

@NgModule({
  declarations: [TooltipComponent, TranslateModalComponent, I18nDirective],
  imports: [CommonModule, MaterialModule, FormsModule, DevelopmentModule],
  providers: [I18nService, UIService, I18nDirective],
  entryComponents: [TooltipComponent, TranslateModalComponent],
  exports: [I18nDirective]
})
export class I18nModule {
  static forRoot(config: LocalizationConfig): ModuleWithProviders {
    return {
      ngModule: I18nModule,
      providers: [{ provide: LocalizationConfig, useValue: config }]
    };
  }
}
