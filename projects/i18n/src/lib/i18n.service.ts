import { Subject } from 'rxjs';
import { Injectable, Optional } from '@angular/core';
import { LocalizationConfig } from './classes/localization.config';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ILanguage } from './interfaces/language.interface';
import { IText } from './interfaces/text.interface';
import { IPage } from './interfaces/page.interface';
import { ITranslation } from './interfaces/translation.interface';
import { Observable } from 'rxjs';
import { UpdateWordDto } from './classes/dtos/update-word.dto';
import { ITranslationPage } from './interfaces/translation-page.interface';
import { FinderHelper } from './classes/finder.helper';
import { IGetTextResponse } from './interfaces/getText-response.interface';
import { AddTranslationDto } from './interfaces/add-translation.dto';
import { environment } from '../../../../src/environments/environment';

@Injectable()
export class I18nService {
  private readonly apiUrl: string;
  private languages: ILanguage[] = [];
  public activeLanguage: ILanguage;
  private selectedLanguage: ILanguage;
  // Subjects
  private languageChanged: Subject<ILanguage> = new Subject<ILanguage>();
  public onLanguageChange = this.languageChanged.asObservable();
  private dataUpdated: Subject<boolean> = new Subject<boolean>();
  public onDataUpdate = this.dataUpdated.asObservable();
  texts: IText[] = [];
  constructor(@Optional() config: LocalizationConfig, private http: HttpClient) {
    this.apiUrl = config.apiUrl;
    this.getInitialData();
  }
  getInitialData() {
    this.getLanguages();
    this.getTexts();
  }
  getLanguages() {
    this.http
      .get<ILanguage[]>(this.getFullUrl('language/all?sort=default&active=true'))
      .subscribe((languages: ILanguage[]) => {
        this.languages = languages;
        this.setActiveLanguage(this.languages[0]);
      });
  }
  getLanguagesFromStore() {
    return this.languages;
  }
  getActiveLanguageGuid(): string {
    return this.selectedLanguage._id;
  }
  getTexts() {
    this.http.get<IText[]>(this.getFullUrl('text/all?preview=false')).subscribe((texts: IText[]) => {
      if (this.texts.length > 0) {
        this.texts = texts;
        this.dataUpdated.next(true);
      } else {
        this.texts = texts;
      }
    });
  }
  getTextFromApi(id: string): Observable<IText> {
    return this.http.get<IText>(this.getFullUrl(`text/${id}`));
  }
  getText(title: string, page: Pick<IPage, 'title' | 'route'>): IGetTextResponse {
    const textFromService = FinderHelper.getCurrentText(this.texts, title);
    if (textFromService) {
      if (textFromService.translations) {
        const currentTranslation = FinderHelper.getCurrentTranslation(textFromService, this.activeLanguage._id);
        const currentPage = FinderHelper.getCurrentPage(currentTranslation, page);
        if (currentPage) {
          return { _id: textFromService._id, value: currentPage.value };
        } else {
          return {
            _id: textFromService._id,
            value: currentTranslation.pages[0].value
          };
          // const addTranslationDto: AddTranslationDto = new AddTranslationDto();
          // addTranslationDto.page = page;
          // addTranslationDto.value = title;
          // this.addPage(textFromService._id, addTranslationDto).subscribe(
          //   (response: any) => console.log(response)
          // );
        }
      }
    } else {
      return { _id: null, value: null };
    }
  }
  addPage(id: string, page: AddTranslationDto) {
    return this.http.patch<IText>(this.getFullUrl(`text/${id}/translation/addPage`), page, this.getOptions());
  }
  dataIsLoaded() {
    return this.texts.length > 0 && this.languages.length > 0;
  }
  private getFullUrl(endpoint: string) {
    return `${this.apiUrl}${endpoint}`;
  }
  // creation, update
  createText(text: Pick<IText, 'title'>, page: Pick<IPage, 'title' | 'route'>): Observable<IText> {
    return this.http
      .post<IText>(
        this.getFullUrl('text/create'),
        {
          title: text.title,
          page
        },
        this.getOptions()
      )
      .pipe(res => {
        return res;
      });
  }
  updateText(text: Pick<IText, '_id'>, translation: ITranslation, page: ITranslationPage): Observable<IText> {
    const updateWordDto: UpdateWordDto = new UpdateWordDto();
    updateWordDto.page = typeof page.page !== 'string' ? page.page : null;
    updateWordDto.translationText = page.value;
    console.log(updateWordDto);
    return this.http.patch<IText>(
      this.getFullUrl(
        `text/${text._id}/translation/${
          typeof translation.languageId !== 'string' ? translation.languageId._id : translation.languageId
        }/update`
      ),
      updateWordDto,
      this.getOptions()
    );
  }
  getOptions(): {
    headers?:
      | HttpHeaders
      | {
          [header: string]: string | string[];
        };
    observe?: 'body';
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  } {
    return {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    };
  }
  // Subjects
  setActiveLanguage(language: ILanguage) {
    this.activeLanguage = language;
    this.selectedLanguage = language;
    this.languageChanged.next(language);
  }
  addText__sub() {
    this.getInitialData();
  }
  updateText__sub() {
    this.getInitialData();
  }
}
