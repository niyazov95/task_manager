import {
  ApplicationRef,
  ComponentFactoryResolver,
  EmbeddedViewRef,
  Injectable,
  Injector,
  Type,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ComponentType } from '@angular/cdk/portal';

@Injectable()
export class UIService {
  constructor(
    private factoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private dialog: MatDialog,
    private injector: Injector
  ) {}
  addTooltip<T>(component: Type<T>) {
    const componentRef = this.factoryResolver
      .resolveComponentFactory<T>(component)
      .create(this.injector);
    this.appRef.attachView(componentRef.hostView);
    const componentRefer = this.factoryResolver
      .resolveComponentFactory<T>(component)
      .create(this.injector);
    this.appRef.attachView(componentRefer.hostView);
    const tooltip = (componentRefer.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;
    document.body.appendChild(tooltip);
    const remove = document.getElementsByTagName('lib-tooltip');
    for (let i = 1; i < remove.length; i++) {
      remove[i].remove();
    }
    // console.log(remove);
  }
  openModal<T>(component: Type<T>, id: string) {
    this.dialog.open(component, {
      data: {
        id,
      },
    });
  }
}
