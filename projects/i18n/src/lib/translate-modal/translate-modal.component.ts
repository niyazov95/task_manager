import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { I18nService } from '../i18n.service';
import { IText } from '../interfaces/text.interface';
import { ITranslation } from '../interfaces/translation.interface';
import { ITranslationPage } from '../interfaces/translation-page.interface';
import { IUser } from '../interfaces/user.interface';
import { IPage } from '../interfaces/page.interface';

@Component({
  selector: 'lib-translate',
  templateUrl: './translate-modal.component.html',
  styleUrls: ['./translate-modal.component.scss'],
})
export class TranslateModalComponent implements OnInit {
  open = true;
  text: IText;
  queryLoading: boolean;
  update: string = '';
  selectedTranslation: ITranslation;
  selectedPage: ITranslationPage;
  constructor(
    private i18nService: I18nService,
    public dialogRef: MatDialogRef<TranslateModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.i18nService
      .getTextFromApi(this.data.id)
      .subscribe((response: IText) => {
        this.text = response;
      });
  }

  selectTranslation(translation: ITranslation) {
    this.selectedTranslation = translation;
    this.selectPage(null);
  }
  selectPage(page: ITranslationPage) {
    this.selectedPage = page;
  }
  isSelectedTranslation(translation: ITranslation): boolean {
    return this.selectedTranslation === translation;
  }
  isSelectedPage(page: ITranslationPage): boolean {
    return this.selectedPage === page;
  }
  closeModal() {
    this.open = false;
    this.dialog.closeAll();
  }
  submit() {
    this.queryLoading = true;
    this.i18nService
      .updateText(
        { _id: this.text._id },
        this.selectedTranslation,
        this.selectedPage
      )
      .subscribe(
        (text: IText) => {
          this.i18nService.updateText__sub();
          this.queryLoading = false;
          this.update = 'UPDATE';
          setTimeout(() => (this.update = ''), 2000);
        },
        (err: any) => {
          console.log(err);
          this.queryLoading = false;
        }
      );
  }
  getPageTitle(page: ITranslationPage) {
    return ((page as ITranslationPage).page as IPage).title;
  }
  getUserFirstName() {
    return (this.text.createdBy.user as IUser).firstName;
  }
  getUserLastName() {
    return (this.text.createdBy.user as IUser).lastName;
  }
}
