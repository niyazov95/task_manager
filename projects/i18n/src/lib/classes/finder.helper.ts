import { ITranslation } from '../interfaces/translation.interface';
import { ITranslationPage } from '../interfaces/translation-page.interface';
import { IPage } from '../interfaces/page.interface';
import { IText } from '../interfaces/text.interface';

export class FinderHelper {
  static getCurrentText(
    texts: IText[],
    title: string
  ): IText {
    return texts.find((text: IText) => text.title === title);
  }
  static getCurrentTranslation(
    text: IText,
    languageId: string
  ): ITranslation {
    return text.translations.find(
      (translation: ITranslation) => translation.languageId === languageId
    );
  }
  static getCurrentPage(
    translation: ITranslation,
    page: Pick<IPage, 'title' | 'route'>
  ): ITranslationPage {
    return translation.pages.find((cPage: ITranslationPage) =>
      this.pageParamsCompression(cPage, page)
    );
  }
  static hashedRoutesUtil(
    page: Pick<IPage, 'title' | 'route'>
  ): Pick<IPage, 'title' | 'route'> {
    return { title: `#${page.title}`, route: `/${page.route}`};
  }
  static pageParamsCompression(
    pageFromArray: ITranslationPage,
    pageToFind: Pick<IPage, 'title' | 'route'>
  ) {
    pageToFind = this.hashedRoutesUtil(pageToFind);
    return (typeof pageFromArray.page !== 'string' ? pageFromArray.page.title === pageToFind.title : null)
      &&
      (typeof pageFromArray.page !== 'string' ? pageFromArray.page.route === pageToFind.route : null);
  }
}
