import { IPage } from '../../interfaces/page.interface';

export class UpdateWordDto {
  translationText: string;
  page: IPage;
}
