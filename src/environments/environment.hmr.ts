export const environment = {
  production: false,
  hmr: true,
  contentUrl: 'http://localhost:3000/'
};
