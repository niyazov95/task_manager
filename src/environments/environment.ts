export const environment = {
  production: false,
  hmr: false,
  contentUrl: 'http://localhost:3000/'
};
