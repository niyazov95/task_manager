export const environment = {
  production: true,
  hmr: false,
  contentUrl: 'http://localhost:3000/'
};
