import { NgModule } from '@angular/core';
import { APIService } from './services/api.service';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarTopComponent } from './components/navbar-top/navbar-top.component';
import { SidenavContentComponent } from './components/sidenav-content/sidenav-content.component';
import { MaterialModule } from '../material';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NotFoundComponent, NavbarTopComponent, SidenavContentComponent],
  providers: [APIService],
  imports: [HttpClientModule, MaterialModule, CommonModule, RouterModule],
  exports: [NotFoundComponent, NavbarTopComponent, SidenavContentComponent]
})
export class CoreModule {}
