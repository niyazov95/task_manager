import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class APIService {
  constructor(private http: HttpClient) {}

  private apiUrl: string = environment.contentUrl;

  static getFullUrl(url: string, apiUrl: string) {
    return `${apiUrl}${url}`;
  }
  get<T>(
    url: string,
    options?: {
      headers?:
        | HttpHeaders
        | {
            [header: string]: string | string[];
          };
      observe?: 'body';
      params?:
        | HttpParams
        | {
            [param: string]: string | string[];
          };
      reportProgress?: boolean;
      responseType?: 'json';
      withCredentials?: boolean;
    },
    apiUrl = this.apiUrl
  ): Observable<T> {
    return this.http.get<T>(APIService.getFullUrl(url, apiUrl), options || undefined);
  }
  post<T>(
    url: string,
    requestBody: any | null,
    options?: {
      headers?:
        | HttpHeaders
        | {
            [header: string]: string | string[];
          };
      observe?: 'body';
      params?:
        | HttpParams
        | {
            [param: string]: string | string[];
          };
      reportProgress?: boolean;
      responseType?: 'json';
      withCredentials?: boolean;
    },
    apiUrl = this.apiUrl
  ): Observable<T> {
    return this.http.post<T>(APIService.getFullUrl(url, apiUrl), requestBody, options || this.getOptions());
  }

  put<T>(
    url: string,
    requestBody: any | null,
    options?: {
      headers?:
        | HttpHeaders
        | {
            [header: string]: string | string[];
          };
      observe?: 'body';
      params?:
        | HttpParams
        | {
            [param: string]: string | string[];
          };
      reportProgress?: boolean;
      responseType?: 'json';
      withCredentials?: boolean;
    },
    apiUrl = this.apiUrl
  ): Observable<T> {
    return this.http.put<T>(APIService.getFullUrl(url, apiUrl), requestBody, options || this.getOptions());
  }

  delete<T>(url: string, elementId?: string | number, apiUrl = this.apiUrl) {
    return this.http.delete<T>(APIService.getFullUrl(`${url}`, apiUrl), this.getOptions());
  }
  getOptions(): {
    headers?:
      | HttpHeaders
      | {
          [header: string]: string | string[];
        };
    observe?: 'body';
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  } {
    return {
      headers: {
        Authorization: ` Bearer ${localStorage.getItem('token')}`
      }
    };
  }
  downloadFileServer(downloadLink: string) {
    return this.http.get(downloadLink, { responseType: 'blob' });
  }
  postFormData<T>(url: string, formData: FormData, apiUrl = this.apiUrl): Observable<T> {
    return this.http.post<T>(APIService.getFullUrl(url, apiUrl), formData);
  }
}
