import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sidenav-content',
  templateUrl: './sidenav-content.component.html',
  styleUrls: ['./sidenav-content.component.scss']
})
export class SidenavContentComponent implements OnInit {
  @Output() $sidenavState = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  toggleSidenav() {
    this.$sidenavState.emit(false);
  }
}
