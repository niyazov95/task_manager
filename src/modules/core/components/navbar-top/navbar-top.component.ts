import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-navbar-top',
  templateUrl: './navbar-top.component.html',
  styleUrls: ['./navbar-top.component.scss']
})
export class NavbarTopComponent implements OnInit {
  route: string;
  @Output() $sidenavState = new EventEmitter<boolean>();
  constructor(location: Location, router: Router) {
    router.events.subscribe(val => {
      if (location.path() === '/task') {
        this.route = 'Task Manager';
      } else {
        this.route = 'Home';
      }
    });
  }

  ngOnInit() {}

  toggleSidenav() {
    this.$sidenavState.emit(true);
  }
}
