import { NgModule } from '@angular/core';
import { I18nWidget } from './i18n-widget/i18n-widget.component';
import { DevelopmentService } from './development.service';
import { LoginModalComponent } from '../../../projects/i18n/src/lib/login-modal/login-modal.component';
import { MaterialModule } from '../material';
import { BottomSheetPanel } from './bottom-sheet-panel/bottom-sheet-panel.component';
import { CoreModule } from '../core/core.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [I18nWidget, LoginModalComponent, BottomSheetPanel],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule],
  providers: [DevelopmentService],
  entryComponents: [I18nWidget, BottomSheetPanel, LoginModalComponent]
})
export class DevelopmentModule {}
