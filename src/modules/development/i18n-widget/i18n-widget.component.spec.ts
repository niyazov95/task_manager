import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { I18nWidget } from './i18n-widget.component';

describe('I18nWidget', () => {
  let component: I18nWidget;
  let fixture: ComponentFixture<I18nWidget>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [I18nWidget]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(I18nWidget);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
