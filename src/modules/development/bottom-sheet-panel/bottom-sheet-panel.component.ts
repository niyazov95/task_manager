import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef, MatDialog } from '@angular/material';
import { DevelopmentService } from '../development.service';
import { LoginModalComponent } from '../../../../projects/i18n/src/lib/login-modal/login-modal.component';

@Component({
  selector: 'app-bottom-sheet-panel',
  templateUrl: './bottom-sheet-panel.component.html',
  styleUrls: ['./bottom-sheet-panel.component.scss']
})
export class BottomSheetPanel implements OnInit {
  public authState: boolean = this.developmentService.getLoginState() || false;

  constructor(
    private bottomSheetRef: MatBottomSheetRef<BottomSheetPanel>,
    private developmentService: DevelopmentService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {}

  login(event: MouseEvent): void {
    this.dialog.open(LoginModalComponent, { width: '500px' });
    this.developmentService.loginAsEditor();
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  logout(event: MouseEvent) {
    this.developmentService.logout();
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
