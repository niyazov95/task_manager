import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomSheetPanel } from './bottom-sheet-panel.component';

describe('BottomSheetPanel', () => {
  let component: BottomSheetPanel;
  let fixture: ComponentFixture<BottomSheetPanel>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BottomSheetPanel]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomSheetPanel);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
