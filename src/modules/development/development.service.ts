import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { APIService } from '../core/services/api.service';

@Injectable()
export class DevelopmentService {
  public loggedAsEditor: boolean;
  public loginState: Subject<boolean> = new Subject<boolean>();
  public onLoginState = this.loginState.asObservable();
  private apiUrl: string = environment.contentUrl;

  constructor(private apiService: APIService) {}

  loginAsEditor(userName?, password?) {
    return this.apiService.post(
      'auth/login',
      {
        userName,
        password
      },
      null,
      this.apiUrl
    );
  }

  logout(): void {
    localStorage.setItem('loggedAsEditor', 'false');
    this.loggedAsEditor = false;
    this.loginState.next(false);
  }

  getLoginState() {
    if (localStorage.getItem('loggedAsEditor') === `${true}`) {
      return true;
    } else {
      return this.loggedAsEditor;
    }
  }
}
