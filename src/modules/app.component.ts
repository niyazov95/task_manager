import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('sidenav', { static: false }) sidenav: MatSidenav;

  constructor(private titleService: Title) {}

  ngOnInit(): void {
    this.titleService.setTitle('QWER');
  }

  toggleSidenav(e) {
    console.log(e);
    this.sidenav.toggle();
  }
}
