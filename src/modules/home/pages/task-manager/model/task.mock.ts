export const TASK = [
  {
    id: 1,
    name:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!',
    priority: { priority: 'High' },
    status: {
      name: 'Done',
      value: 'check_circle_outline'
    },
    date: '14.03.2020',
    type: { type: 'Front-end' },
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!'
  },
  {
    id: 1,
    name:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!',
    priority: { priority: 'Low' },
    status: {
      name: 'Canceled',
      value: 'cancel'
    },
    date: '14.03.2020',
    type: { type: 'Front-end' },
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!'
  },
  {
    id: 1,
    name:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!',
    priority: { priority: 'Normal' },
    status: {
      name: 'On Hold',
      value: 'timer'
    },
    date: '14.03.2020',
    type: { type: 'Back-end' },
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!'
  },
  {
    id: 1,
    name:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!',
    priority: { priority: 'High' },
    status: {
      name: 'Done',
      value: 'check_circle_outline'
    },
    date: '14.03.2020',
    type: { type: 'Front-end' },
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!'
  },
  {
    id: 1,
    name:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!',
    priority: { priority: 'Low' },
    status: {
      name: 'Canceled',
      value: 'cancel'
    },
    date: '14.03.2020',
    type: { type: 'Front-end' },
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!'
  },
  {
    id: 1,
    name:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!',
    priority: { priority: 'Normal' },
    status: {
      name: 'On Hold',
      value: 'timer'
    },
    date: '14.03.2020',
    type: { type: 'Back-end' },
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto dignissimos eaque earum explicabo facilis fugiat iste maiores nam, placeat!'
  }
];
