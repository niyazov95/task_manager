import { TaskPriorityEnum, TaskStatusEnum } from './task.enum';
import { TaskPriorityModel } from '../task-card/task-actions/model/TaskPriorityModel';
import { TaskStatusModel } from '../task-card/task-actions/model/TaskStatusModel';
import { TaskTypeModel } from '../task-card/task-actions/model/TaskTypeModel';

export class TaskModel {
  id: number;
  name: string;
  priority: TaskPriorityModel;
  status: TaskStatusModel;
  type: TaskTypeModel;
  date: string;
  description: string;
}
