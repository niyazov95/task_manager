export enum TaskStatusEnum {
  Done = 0,
  InProgress = 1,
  OnHold = 2,
  Canceled = 3
}

export enum TaskPriorityEnum {
  High = 0,
  'Low' = 1,
  Normal = 2,
  VeryHigh = 3
}
