export const TASK_PRIORITY = [
  {
    priority: 'High'
  },
  {
    priority: 'Low'
  },
  {
    priority: 'Normal'
  }
];
