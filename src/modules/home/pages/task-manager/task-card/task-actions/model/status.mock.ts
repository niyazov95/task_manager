export const TASK_STATUS = [
  {
    name: 'Done',
    value: 'check_circle_outline'
  },
  {
    name: 'Cancel',
    value: 'cancel'
  },
  {
    name: 'On Hold',
    value: 'timer'
  }
];
