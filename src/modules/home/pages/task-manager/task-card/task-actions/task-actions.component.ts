import { Component, Input, OnInit } from '@angular/core';
import { TaskModel } from '../../model/task.model';
import { TaskStatusModel } from './model/TaskStatusModel';
import { TASK_STATUS } from './model/status.mock';
import { TaskPriorityModel } from './model/TaskPriorityModel';
import { TaskTypeModel } from './model/TaskTypeModel';
import { TASK_PRIORITY } from './model/TaskPriorityModel.mock';
import { TASK_TYPE } from './model/tasktype.mock';

@Component({
  selector: 'app-task-actions',
  templateUrl: './task-actions.component.html',
  styleUrls: ['./task-actions.component.scss']
})
export class TaskActionsComponent implements OnInit {
  @Input() task: TaskModel;
  status: TaskStatusModel[];
  priority: TaskPriorityModel[];
  type: TaskTypeModel[];

  constructor() {}

  ngOnInit() {
    this.status = TASK_STATUS;
    this.priority = TASK_PRIORITY;
    this.type = TASK_TYPE;
  }
}
