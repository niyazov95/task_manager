import { TaskStatusModel } from '../task-actions/model/TaskStatusModel';
import { TaskTypeModel } from '../task-actions/model/TaskTypeModel';
import { TaskPriorityModel } from '../task-actions/model/TaskPriorityModel';

export class AddTaskDto {
  id: number;
  name: string;
  status: TaskStatusModel;
  priority: TaskPriorityModel;
  type: TaskTypeModel;
  date: string;
  description: string;
}
