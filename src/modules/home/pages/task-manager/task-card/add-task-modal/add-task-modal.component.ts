import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { TASK_STATUS } from '../task-actions/model/status.mock';
import { TASK_PRIORITY } from '../task-actions/model/TaskPriorityModel.mock';
import { TASK_TYPE } from '../task-actions/model/tasktype.mock';
import { TaskStatusModel } from '../task-actions/model/TaskStatusModel';
import { TaskPriorityModel } from '../task-actions/model/TaskPriorityModel';
import { TaskTypeModel } from '../task-actions/model/TaskTypeModel';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

class DialogData {}

@Component({
  selector: 'app-add-task-modal',
  templateUrl: './add-task-modal.component.html',
  styleUrls: ['./add-task-modal.component.scss']
})
export class AddTaskModalComponent implements OnInit {
  status: TaskStatusModel[];
  priority: TaskPriorityModel[];
  type: TaskTypeModel[];
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, private builder: FormBuilder) {}
  // Form builder
  taskForAdding: FormGroup = this.builder.group({
    id: [1, Validators.required],
    name: ['', Validators.required],
    status: {
      value: ['', Validators.required],
      name: ['', Validators.required]
    },
    priority: {
      priority: ['', Validators.required]
    },
    description: ['', Validators.required],
    type: {
      type: ['', Validators.required]
    },
    date: [new Date(), Validators.required]
  });

  ngOnInit(): void {
    this.status = TASK_STATUS;
    this.priority = TASK_PRIORITY;
    this.type = TASK_TYPE;
  }
}
