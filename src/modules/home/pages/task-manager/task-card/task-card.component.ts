import { Component, OnInit } from '@angular/core';
import { TaskModel } from '../model/task.model';
import { TASK } from '../model/task.mock';
import { MatDialog } from '@angular/material';
import { AddTaskModalComponent } from './add-task-modal/add-task-modal.component';

@Component({
  selector: 'app-task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.scss']
})
export class TaskCardComponent implements OnInit {
  task: TaskModel[];

  constructor(private dialog: MatDialog) {}

  ngOnInit() {
    this.task = TASK;
    console.log(this.task);
  }

  addTask() {
    const dialog = this.dialog.open(AddTaskModalComponent, {
      width: '800px',
      data: {
        task: this.task
      }
    });
    dialog.afterClosed().subscribe(
      (r: any) => {
        console.log(r);
        this.task.push(r.value);
      },
      error => console.log(error)
    );
  }
}
