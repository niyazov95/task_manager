import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { I18nModule } from '../../../projects/i18n/src/lib/i18n.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material';
import { TaskManagerComponent } from './pages/task-manager/task-manager.component';
import { TaskCardComponent } from './pages/task-manager/task-card/task-card.component';
import { TaskActionsComponent } from './pages/task-manager/task-card/task-actions/task-actions.component';
import { AddTaskModalComponent } from './pages/task-manager/task-card/add-task-modal/add-task-modal.component';

@NgModule({
  declarations: [
    HomepageComponent,
    TaskManagerComponent,
    TaskCardComponent,
    TaskActionsComponent,
    AddTaskModalComponent
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, HomeRoutingModule, I18nModule, MaterialModule],
  entryComponents: [AddTaskModalComponent]
})
export class HomeModule {}
